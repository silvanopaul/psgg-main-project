<?php

namespace Database\Factories;

use App\Models\Activity;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class ActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Activity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title(),
            'short_description' => $this->faker->realText(),
            'description' => $this->faker->sentence(5, true),
            'featured_image' => $this->faker->randomElement(array('/uploads/image1.png', '/uploads/image2.png', '/uploads/image3.png')),
            'is_featured' => $this->faker->boolean()
        ];
    }
}
