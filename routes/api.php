<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\GalleryAlbumController;
use App\Http\Controllers\HomepageBannerController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/', [AdminController::class, 'checkUser']);
// Route::get('{slug}', [AdminController::class, 'notFound']);

// ADMIN ROUTES
// Activities
Route::get('/activities', [ActivityController::class, 'getAllActivities']);
Route::get('/activity/{id}', [ActivityController::class, 'getActivity']);
Route::post('/activity', [ActivityController::class, 'createActivity']);
Route::patch('/activity/{id}', [ActivityController::class, 'updateActivity']);
Route::delete('/activity/{id}', [ActivityController::class, 'deleteActivity']);

Route::delete('/activity_image/{id}', [ActivityController::class, 'deleteActivityImage']);

// Roles
Route::get('/roles', [UserController::class, 'getAllRoles']);
Route::get('/role/{id}', [UserController::class, 'getRole']);
Route::post('/role', [UserController::class, 'createRole']);
Route::patch('/role/{id}', [UserController::class, 'updateRole']);
Route::delete('/role/{id}', [UserController::class, 'deleteRole']);

// Users
Route::get('/users', [UserController::class, 'getAllUsers']);
Route::get('/user/{id}', [UserController::class, 'getUser']);
Route::post('/user', [UserController::class, 'createUser']);
Route::patch('/user/{id}', [UserController::class, 'updateUser']);
Route::delete('/user/{id}', [UserController::class, 'deleteUser']);

// About Sections
Route::get('/about_sections', [AboutController::class, 'getAboutSections']);
Route::get('/about_section/{id}', [AboutController::class, 'getAboutSection']);
Route::post('/about_section', [AboutController::class, 'createAboutSection']);
Route::patch('/about_section/{id}', [AboutController::class, 'updateAboutSection']);
Route::delete('/about_section/{id}', [AboutController::class, 'deleteAboutSection']);

// About Panels
Route::get('/about_panels', [AboutController::class, 'getAboutPanels']);
Route::get('/about_panel/{id}', [AboutController::class, 'getAboutPanel']);
Route::post('/about_panel', [AboutController::class, 'createAboutPanel']);
Route::patch('/about_panel/{id}', [AboutController::class, 'updateAboutPanel']);
Route::delete('/about_panel/{id}', [AboutController::class, 'deleteAboutPanel']);

// About Panel Sections
Route::post('/about_panel_section', [AboutController::class, 'createAboutPanelSection']);
Route::patch('/about_panel_section/{id}', [AboutController::class, 'updateAboutPanelSection']);
Route::delete('/about_panel_section/{id}', [AboutController::class, 'deleteAboutPanelSection']);

// CATALOG ROUTES
// Navbar, Contact Page
Route::get('/cat_setting', [SettingController::class, 'getSettings']);
// Gallery Page
Route::get('/cat_albums', [GalleryAlbumController::class, 'getAlbums']);
Route::get('/cat_album/{id}', [GalleryAlbumController::class, 'getAlbum']);

// Activity Page
Route::get('/cat_activities', [ActivityController::class, 'getActivities']);
Route::get('/cat_activity/{id}', [ActivityController::class, 'getActivity']);

// About Page
Route::get('/cat_sections', [AboutController::class, 'getAboutSections']);
Route::get('/cat_panels', [AboutController::class, 'getAboutPanels']);

// Home Page
Route::get('/cat_banners', [HomepageBannerController::class, 'getHomeBanners']);
Route::get('/cat_featured_activities', [ActivityController::class, 'getFeaturedActivities']);


// User Login
Route::post('/user_login', [AdminController::class, 'userLogin']);





