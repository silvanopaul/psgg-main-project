<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>


        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">

        <!-- CSS Styles -->
        <link rel="stylesheet" href="/css/all.css">

        <!-- Magnific Popup core JS file -->
        <!-- <script src="./assets/scripts/jquery.magnific-popup.js"></script> -->
        
        <style>
            body {
                font-family: 'Roboto', sans-serif !important;
            }
        </style>
    </head>
    <body>
    <!-- <body class="antialiased"> -->
        <!-- <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/home') }}" class="text-sm text-gray-700 underline">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        </div> -->
        <div id="app">
            <main-app></main-app>
        </div>
        <script>
            var BASE_URL = '{{ URL::to('/')  }}';
        </script>
        <script src="{{mix('/js/app.js')}}"></script>
        <script src="{{ mix('/js/all.js') }}"></script>
    </body>
</html>
