<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;

class SettingController extends Controller
{
    public function getSettings(){
        $settings = Setting::with('social_media')->find(1);
        return $settings;        
    }

    public function saveSettings(Request $request){
        $settings = Setting::find(1);

        $rules = [
            "logo" => "nullable",
            "name" => "required",
            "email" => "required",
            "contact" => "nullable|regex:/^[a-zA-Z0-9-.,+()\n ]+$/|max:255",
            "address" => "nullable|regex:/^[a-zA-Z0-9-.,+\n ]+$/|max:255",
            "google_map" => "nullable|max:1000",
        ];
        $this->validate($request, $rules);
        $settings->logo = $request->logo;  
        $settings->name = $request->name;  
        $settings->email = $request->email;  
        $settings->contact_number = $request->contact;
        $settings->address = $request->address;  
        $settings->google_map = $request->google_map;  
        $settings->save();

        return 'saved';
    }
}
