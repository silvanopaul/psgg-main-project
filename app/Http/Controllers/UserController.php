<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    // Roles Controller
    public function getAllRoles(){
        $roles = Role::all();
        return response()->json($roles, 200);
    }
    
    public function createRole(Request $request){
        $rules = array(
            'name' => 'required',
        );

        $this->validate($request, $rules);
        $role = new Role;
        $role->name = $request->name;
        $role->save();

        return response()->json($role, 200);
    }

    public function getRole($id){
        $role = Role::find($id);
        return response()->json($role, 200);
    }

    public function updateRole($id, Request $request){
        $role = Role::find($id);
        $rules = array(
            'name' => 'required',
        );
        $this->validate($request, $rules);
        $role->name = $request->name;
        $role->save();

        return response()->json($role, 200);
    }

    public function deleteRole($id){
        $role_to_delete = Role::find($id);
        $role_to_delete->delete();

        return response()->json($role_to_delete, 200);
    }

    // Users Controller
    public function getAllUsers(){
        $users = User::with('role')->orderBy('created_at', 'desc')->paginate(15);
        return response()->json($users, 200);
    }

    public function getUser($id){
        $user = User::find($id);
        return response()->json($user, 200);
    }

    public function createUser(Request $request){
        $rules = array(
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'role_id' => 'required',
        );
        $this->validate($request, $rules);
        $user = new User;
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $destination = "/uploads/";
        $user->image_path = is_null($request->image_path) ? '/uploads/no_image.jpg' : $destination.$request->image_path;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->save();
        return response()->json($user, 200);
    }

    public function updateUser($id, Request $request){
        $rules = array(
            'name' => "required|string|max:255",            
            'last_name' => "required|string|max:255",
            'email' => "required|string|email|max:255|unique:users,email,$request->id",
        );
        $this->validate($request, $rules);
        $user = User::find($id);
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $destination = "/uploads/";
        if ($request->image_path != $user->image_path) {
            $user->image_path = is_null($request->image_path) ? '/uploads/no_image.jpg' : $destination.$request->image_path;
        }
        $user->role_id = $request->role_id;
        $user->save();
        return response()->json($user, 200);
    }

    public function deleteUser($id){
        $user_to_delete = User::find($id);
        $user_to_delete->delete();
        return response()->json(200);
    }
}
