<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\GalleryImage;
use App\Models\GalleryAlbum;

class GalleryImageController extends Controller
{
    public function download($id)
    {
        $image = GalleryImage::find($id);
        return Storage::download($image->path);
    }

    public function addGalleryImage(Request $request)
    {
        $rules = array(
            'name' => 'required',
        );

        $this->validate($request, $rules);
        $new_image = new GalleryImage;
        $new_image->name = $request->name;
        $new_image->description = $request->description;
        $destination = "/uploads/";
        $new_image->image_path = $destination.$request->image_path;
        $new_image->sort_order = $request->sort_order;
        $new_image->gallery_album_id = $request->gallery_album_id;
        $new_image->save();
        return response()->json($new_image, 200);
    }

    public function saveGalleryImage(Request $request)
    {
        $image = GalleryImage::find($request->id);
        $rules = array(
            'name' => 'required',
        );

        $this->validate($request, $rules);
        $image->name = $request->name;
        $image->description = $request->description;
        $image->image_path = $request->image_path;
        $image->sort_order = $request->sort_order;
        $image->save();
        return response()->json($image, 200);
    }
    
    public function deleteGalleryImage(Request $request)
    {
        $image_to_delete = GalleryImage::find($request->id);
        $image_to_delete->delete();

        return response()->json($image_to_delete, 200);
    }
}
