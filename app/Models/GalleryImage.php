<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'image_path',
        'sort_order',
        'gallery_album_id',
    ];

    public function gallery_albums() {
        return $this->belongsTo(GalleryAlbum::class);
    }
}
