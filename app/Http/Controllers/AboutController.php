<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AboutSection;
use App\Models\AboutPanel;
use App\Models\AboutPanelSection;

class AboutController extends Controller
{
    // About Sections
    public function getAboutSections()
    {
        $sections = AboutSection::orderBy('created_at','asc')->get();;
        return response()->json($sections, 200);
    }

    public function getAboutSection($id){
        $section = AboutSection::find($id);
        return response()->json($section, 200);
    }

    public function createAboutSection(Request $request)
    {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|max:255',
            'description' => 'nullable|max:3000',            
        );
        $this->validate($request, $rules);
        $new_section = new AboutSection;
        $new_section->title = $request->title;
        $new_section->description = $request->description;
        $destination = "/uploads/";
        $new_section->image_path = is_null($request->image_path) ? '/uploads/no_image.jpg' : $destination.$request->image_path;
        $new_section->about_id = 1;
        $new_section->save();
        return response()->json($new_section, 200);
    }

    public function updateAboutSection($id, Request $request)
    {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|max:255',
            'description' => 'nullable|max:3000',            
        );
        $this->validate($request, $rules);
        $section = AboutSection::find($id);
        $section->title = $request->title;
        $section->description = $request->description;
        $destination = "/uploads/";
        if ($request->image_path != $section->image_path) {
            $section->image_path = is_null($request->image_path) ? '/uploads/no_image.jpg' : $destination.$request->image_path;
        }
        $section->save();
        return response()->json($section, 200);
    }

    public function deleteAboutSection($id)
    {
        $section_to_delete = AboutSection::find($id);
        $section_to_delete->delete();
        return response()->json('section has been deleted', 200);
    }

    // About Panels
    public function getAboutPanels()
    {
        $panels = AboutPanel::with('about_panel_sections')->orderBy('created_at','asc')->get();;
        return response()->json($panels, 200);
    }

    public function createAboutPanel(Request $request)
    {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|max:255',        
        );
        $this->validate($request, $rules);
        $new_panel = new AboutPanel;
        $new_panel->title = $request->title;
        $new_panel->about_id = 1;
        $new_panel->save();
        return response()->json($new_panel, 200);
    }

    public function updateAboutPanel($id, Request $request)
    {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|max:255',        
        );
        $this->validate($request, $rules);
        $panel = AboutPanel::find($id);
        $panel->title = $request->title;
        $panel->save();
        return response()->json($panel, 200);
    }

    public function deleteAboutPanel($id)
    {
        $panel_to_delete = AboutPanel::find($id);
        $panel_to_delete->delete();
        return response()->json('Panel has been deleted', 200);
    }

    // About Panel Sections
    public function createAboutPanelSection(Request $request)
    {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|max:255',
            'description' => 'nullable|max:3000',            
        );
        $this->validate($request, $rules);

        $panel = AboutPanel::find($request->about_panel_id);
        $destination = "/uploads/";
        $panel->about_panel_sections()->create([
            'title' => $request->title,
            'description' => $request->description,
            'image_path' => is_null($request->image_path) ? '/uploads/no_image.jpg' : $destination.$request->image_path,
            'about_id' => 1,
        ]);
        return response()->json($panel, 200);
    }

    public function updateAboutPanelSection($id, Request $request)
    {
        $rules = array(
            'title' => 'required|string|regex:/^[a-zA-Z0-9\s, ]+$/|max:255',
            'description' => 'nullable|max:3000',            
        );
        $this->validate($request, $rules);
        $section = AboutPanelSection::find($id);
        $section->title = $request->title;
        $section->description = $request->description;
        $destination = "/uploads/";
        if ($request->image_path != $section->image_path) {
            $section->image_path = is_null($request->image_path) ? '/uploads/no_image.jpg' : $destination.$request->image_path;
        }
        $section->save();
        return response()->json($section, 200);
    }

    public function deleteAboutPanelSection($id)
    {
        $panel_section_to_delete = AboutPanelSection::find($id);
        $panel_section_to_delete->delete();
        return response()->json('Panel section has been deleted', 200);
    }
}
