<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\ActivityImage;

class ActivityController extends Controller
{
    public function getAllActivities()
    {
        $activities = Activity::with('activity_images')->orderBy('created_at', 'desc')->paginate(5);
        foreach($activities as $activity){
            $activity['create'] = $activity->created_at->diffForHumans();
        }
        return response()->json($activities, 200);
    }

    public function createActivity(Request $request) {
        $rules = array(
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:3000',
        );
        $this->validate($request, $rules);
        $destination = "/uploads/";
        $activity = Activity::create([
            'title' => $request->title,
            'short_description' => $request->short_description,
            'description' => $request->description,
            'featured_image' => is_null($request->featured_image) ? '/uploads/no_image.jpg' : $destination.$request->featured_image,
            'is_featured' => $request->is_featured ? $request->is_featured : 0,
        ]);

        $images = $request->get('images');
        $activity->activity_images()->createMany($images);

        return response()->json($activity, 200);
    }

    public function getActivity($id)
    {
        $activity = Activity::with('activity_images')->find($id);
        return response()->json($activity, 200);
    }

    public function updateActivity($id, Request $request)
    {
        $activity = Activity::with('activity_images')->find($id);
        $rules = array(
            'title' => 'required',
            'description' => 'required',
        );
        $this->validate($request, $rules);
        $activity->title = $request->title;
        $activity->short_description = $request->short_description;
        $activity->description = $request->description;
        $destination = "/uploads/";

        if (is_null($request->featured_image)) {
            $activity->featured_image = $destination."no_image.jpg";
        } else {
            if(str_contains($request->featured_image, $destination)) {
                $activity->featured_image = $request->featured_image;
            } else {
                $activity->featured_image = $destination.$request->featured_image;
            }
        }

        $images = $request->get('images');
        $activity->activity_images()->createMany($images);

        $activity->is_featured = $request->is_featured ? $request->is_featured : 0;
        $activity->save();

        return response()->json($activity, 200);
    }

    public function deleteActivity($id)
    {
        $activity_to_delete = Activity::find($id);
        $activity_to_delete->delete();
        return response()->json('deleted', 200);
    }

    public function deleteActivityImage($id)
    {
        $image_to_delete = ActivityImage::find($id);
        $image_to_delete->delete();
        return response()->json('deleted', 200);
    }

    // Catalog Controller
    public function getActivities()
    {
        $activities = Activity::with('activity_images')->orderBy('created_at', 'desc')->paginate(12);
        foreach($activities as $activity){
            $activity['create'] = $activity->created_at->diffForHumans();
            $activity['latest_image'] = ActivityImage::where('activity_id', $activity->id)->latest()->first();
        }
        return response()->json($activities, 200);
    }

    public function getFeaturedActivities()
    {
        $activities = Activity::with('activity_images')->orderBy('created_at', 'desc')->where('is_featured', '1')->get();
        foreach($activities as $activity){
            $activity['create'] = $activity->created_at->diffForHumans();
            $activity['latest_image'] = ActivityImage::where('activity_id', $activity->id)->latest()->first();
        }
        return response()->json($activities, 200);
    }
}