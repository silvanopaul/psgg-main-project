<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\BoardMemberController;
use App\Http\Controllers\HomepageBannerController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SocialMediaController;
use App\Http\Controllers\GalleryAlbumController;
use App\Http\Controllers\GalleryImageController;
use App\Http\Controllers\ActivityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Auth::routes();

// Admin
Route::post('app/upload', [AdminController::class, 'uploadFile']);
Route::post('app/multi_upload', [AdminController::class, 'uploadMultipleFiles']);
Route::post('app/delete_image', [AdminController::class, 'deleteFile']);
Route::get('app/get_board_members', [BoardMemberController::class, 'showAllBoardMembers']);
Route::post('app/add_board_member', [BoardMemberController::class, 'addBoardMember']);
Route::patch('app/edit_board_member', [BoardMemberController::class, 'saveEditedBoardMember']);
Route::delete('app/delete_board_member', [BoardMemberController::class, 'deleteBoardMember']);

Route::get('app/get_home_banners', [HomepageBannerController::class, 'showAllHomeBanners']);
Route::post('app/add_home_banner', [HomepageBannerController::class, 'addHomeBanner']);
Route::patch('app/edit_home_banner', [HomepageBannerController::class, 'saveEditedHomeBanner']);
Route::delete('app/delete_home_banner', [HomepageBannerController::class, 'deleteHomeBanner']);

Route::get('app/get_settings', [SettingController::class, 'getSettings']);
Route::patch('app/edit_settings', [SettingController::class, 'saveSettings']);
Route::get('app/get_social_medias', [SocialMediaController::class, 'showAllSocials']);
Route::post('app/add_social_media', [SocialMediaController::class, 'addSocial']);
Route::patch('app/edit_social_media', [SocialMediaController::class, 'saveEditedSocial']);
Route::delete('app/delete_social_media', [SocialMediaController::class, 'deleteSocial']);

Route::get('app/get_albums', [GalleryAlbumController::class, 'showAllGalleryAlbums']);
Route::post('app/add_album', [GalleryAlbumController::class, 'addGalleryAlbum']);
Route::post('app/get_album', [GalleryAlbumController::class, 'showGalleryAlbum']);
Route::patch('app/edit_album', [GalleryAlbumController::class, 'saveEditedGalleryAlbum']);
Route::delete('app/delete_album', [GalleryAlbumController::class, 'deleteGalleryAlbum']);

Route::post('app/add_album_image', [GalleryImageController::class, 'addGalleryImage']);
Route::patch('app/edit_album_image', [GalleryImageController::class, 'saveGalleryImage']);
Route::delete('app/delete_album_image', [GalleryImageController::class, 'deleteGalleryImage']);

Route::get('app/get_activities', [ActivityController::class, 'showAllActivities']);
Route::post('app/add_activity', [ActivityController::class, 'addActivity']);
Route::post('app/get_activity', [ActivityController::class, 'showActivity']);
Route::patch('app/edit_activity', [ActivityController::class, 'saveEditedActivity']);
Route::delete('app/delete_activity', [GalleryAlbumController::class, 'deleteGActivity']);

Auth::routes();

// Route::get('/', [AdminController::class, 'checkUser']);
Route::get('{slug}', [AdminController::class, 'notFound']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
