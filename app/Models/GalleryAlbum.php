<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GalleryAlbum extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'album_image',
    ];
    
    public function gallery_images(){
        return $this->hasMany(GalleryImage::class);
    }
}
