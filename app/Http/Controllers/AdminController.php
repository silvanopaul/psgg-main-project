<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    // public function checkUser(Request $request)
    // {
    //     if (!Auth::check()) {
    //         return redirect('/home');
    //     } else {
            
    //     }
    // }

    public function notFound(){
        return view('/app');
    }

    public function uploadFile(Request $request)
    {
        $rules = array(
            "file" => "required|image|mimes:jpeg,jpg,png,gif|max:2048",
        );
        // Validate request
        $this->validate($request, $rules);
        // $picName = time().'.'.$request->file->extension();
        // $request->file->move(public_path('uploads'), $picName);
        // return $picName;
        $random = Str::random(5);
        $image = $request->file;
        $image_name = $random . time() . "." . $image->extension();
        $image->move(public_path("uploads"), $image_name);

        return $image_name;
    }

    public function uploadMultipleFiles(Request $request)
    {
        $rules = array(
            "file" => "required|image|mimes:jpeg,jpg,png,gif|max:2048",
        );
        $this->validate($request, $rules);
        $random = Str::random(5);
        $destination = "/uploads/";
        $image = $request->file;
        $image_original_name = $image->getClientOriginalName();
        $image_name = $destination . $random . time() . "." . $image->extension();
        $image->move(public_path("uploads"), $image_name);
        $image_holder = array(
            "name" => $image_original_name,
            "image_path" => $image_name,
            "sort_order" => 0,
        );
        return $image_holder;
    }

    public function deleteFile(Request $request)
    {
        $fileName = $request->imageName;
        $this->deleteFileFromServer($fileName, false);
        return;
    }

    public function deleteFileFromServer($fileName, $hasFullPath = false)
    {
        if (!$hasFullPath) {
            $filePath = public_path() . '/uploads/' . $fileName;
        }
        if (file_exists($filePath)) {
            @unlink($filePath);
        }
        return;
    }

    // public function userLogin(Request $request)
    // {
    //     $rules = array(
    //         'email' => 'bail|required|email',
    //         'password' => 'bail|required|min:8',
    //     );
    //     $this->validate($request, $rules);
    //     $credentials = $request->only('email', 'password');

    //     if (Auth::attempt($credentials)) {
    //         $user = Auth::user();
    //         $role = Role::find($user->role_id);

    //         if ($role->name != 'Admin') {
    //             Auth::logout();
    //             return response()->json(['msg' => 'Incorrect login details'], 401);
    //         } else {
    //             // $request->session()->regenerate();
    //             // $value = $request->session()->get('key', 'default');
    //             return response()->json(['msg' => 'You are logged in'], 200);
    //         }
    //     } else {
    //         return response()->json(['msg' => 'Incorrect login details'], 401);
    //     }
    // }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return response()->json(200);
    }
}
