require('./bootstrap');
window.Vue = require('vue').default;
import store from './store';
import router from './router';
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css'; 
Vue.use(ViewUI);

import common from './common';
Vue.mixin(common); // can be use in any component globally

Vue.component('main-app', require('./components/MainApp.vue').default);
// Vue.component('home-page', require('./components/pages/HomePage.vue'));

const app = new Vue({
    el: '#app',
    router,
    store
});
