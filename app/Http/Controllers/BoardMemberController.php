<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BoardMember;


class BoardMemberController extends Controller
{
    public function showAllBoardMembers(){
        $members = BoardMember::orderBy('id','desc')->get();
        return response()->json($members,200);
    }

    public function addBoardMember(Request $request){
        $rules = array(
            "name" => "required",
            "position" => "required",
            // "image_path" => "required|image|mimes:jpeg,jpg,png,gif|max:2048",
        );

        $this->validate($request, $rules);
        
        $new_member = new BoardMember;
        $new_member->name = $request->name;
        $new_member->position = $request->position;
        $new_member->description = $request->description;
        $destination = "/uploads/";
        $new_member->image_path = $destination.$request->image_path;

        // Saving image file
        // $image = $request->file('image');
        // $image_name = time().".".$image->getClientOriginalExtension();
        // $image->move(public_path($destination), $image_name);

        $new_member->save();
        return response()->json($new_member,200);     
    }

    public function saveEditedBoardMember(Request $request){
        $member = BoardMember::find($request->id);

        $rules = array(
            "name" => "required",
            "position" => "required",
            // "image_path" => "required|mimes:jpeg,jpg,png,gif|max:2048",
        );

        $this->validate($request, $rules);
        
        $member->name = $request->name;
        $member->position = $request->position;
        $member->description = $request->description;
        $member->image_path = $request->image_path;       
        $member->save();
        return response()->json($member,200);
    }

    public function deleteBoardMember(Request $request){
        $memberToDelete = BoardMember::find($request->id);
        $memberToDelete->delete();

        return $memberToDelete;  
    }
}
