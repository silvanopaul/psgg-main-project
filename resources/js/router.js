import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// Catalog
import catalogHomePage from './catalog/pages/HomePage'
import catalogAboutPage from './catalog/pages/AboutPage'
import catalogActivityPage from './catalog/pages/ActivityPage'
import catalogActivityInnerPage from './catalog/pages/ActivityInnerPage'
import catalogGalleryPage from './catalog/pages/GalleryPage'
import catalogGalleryInnerPage from './catalog/pages/GalleryInnerPage'
import catalogContactPage from './catalog/pages/ContactPage'
import userLoginPage from './catalog/pages/Login'

// Admin
import dashboardPage from './admin/pages/Dashboard'
import boardMemberPage from './admin/pages/BoardMember'
import homeBannerPage from './admin/pages/HomepageBanner'
import settingPage from './admin/pages/Setting'
import socialMediaPage from './admin/pages/SocialMedia'
import deleteModal from './admin/components/DeleteModal'
import galleryAlbumPage from './admin/pages/GalleryAlbum'
import albumShow from './admin/pages/AlbumShow'
import activityPage from './admin/pages/Activity'
import userPage from './admin/pages/User'
import aboutSectionPage from './admin/pages/AboutSection'
import aboutPanelPage from './admin/pages/AboutPanel'

const routes = [
    {
        path: '/home',
        component: catalogHomePage,
        name: 'home'
    },
    {
        path: '/about-us',
        component: catalogAboutPage,
        name: 'about'
    },
    {
        path: '/activity',
        component: catalogActivityPage,
        name: 'activity'
    },
    {
        path: '/activity',
        component: catalogActivityInnerPage,
        name: 'activity.inner'
    },
    {
        path: '/gallery',
        component: catalogGalleryPage,
        name: 'gallery'
    },
    {
        path: '/gallery',
        component: catalogGalleryInnerPage,
        name: 'gallery.inner'
    },
    {
        path: '/contact-us',
        component: catalogContactPage,
        name: 'contact'
    },
    {
        path: '/login',
        component: userLoginPage,
        name: 'user.login'
    },

    // Admin Pages
    {
        path: '/dashboard',
        component: dashboardPage,
        name: 'dashboard'
    },
    {
        path: '/board-members',
        component: boardMemberPage,
        name: 'about.boards'
    },
    {
        path: '/about-sections',
        component: aboutSectionPage,
        name: 'about.sections'
    },
    {
        path: '/about-panels',
        component: aboutPanelPage,
        name: 'about.panels'
    },
    {
        path: '/home-banners',
        component: homeBannerPage,
        name: 'home.banners'
    },
    {
        path: '/settings-general',
        component: settingPage,
        name: 'settings.general'
    },
    {
        path: '/settings-social-media',
        component: socialMediaPage,
        name: 'settings.socials'
    },
    {
        path: '/delete-modal',
        component: deleteModal
    },
    {
        path: '/galleries',
        component: galleryAlbumPage,
        name: 'gallery.albums'
    },
    {
        path: '/galleries-album',
        component: albumShow,
        name: 'album.show'
    },
    {
        path: '/activities',
        component: activityPage,
        name: 'activities'
    },
    {
        path: '/users',
        component: userPage,
        name: 'users'
    },
];

export default new Router({
    mode: 'history',
    routes
})