<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">

     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <script>
          window.Laravel = {
               csrfToken: '{{ csrf_token() }}'
          }
     </script>


     <title>Laravel</title>

     <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

     <!-- CSS Styles -->
     <link rel="stylesheet" href="{{ mix('/css/all.css') }}">

</head>

<body>
     <div id="app" class="flex-center position-ref full-height">
          <main-app></main-app>
     </div>
     <!-- <script>
          var BASE_URL = '{{ URL::to(' / ')  }}';
          </script> -->
     <script src="{{ mix('/js/app.js') }}"></script>
     <script src="{{ mix('/js/all.js') }}"></script>
</body>

</html>