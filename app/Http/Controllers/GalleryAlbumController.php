<?php

namespace App\Http\Controllers;

use App\Models\GalleryAlbum;
use App\Models\GalleryImage;
use Illuminate\Http\Request;

class GalleryAlbumController extends Controller
{
    public function showAllGalleryAlbums()
    {
        // $user = Auth::user();
        $albums = GalleryAlbum::orderBy('id','desc')->get();
        foreach($albums as $album)
        {
            $album['latestImage'] = GalleryImage::where('gallery_album_id', $album->id)->latest()->first();
        }
        return response()->json($albums, 200);
    }

    public function addGalleryAlbum(Request $request)
    {
        $rules = array();
        $this->validate($request, $rules);
        $album = new GalleryAlbum;
        $album->name = $request->name;
        $album->description = $request->description;
        $destination = "/uploads/";
        $album->album_image = $destination.$request->album_image;
        $album->save();

        return response()->json($album, 200);
    }

    public function showGalleryAlbum(Request $request)
    {
        // WITH IMAGE SORT ORDER ASCENDING FOR FRONTEND
        
        // $album = GalleryAlbum::with(['gallery_images' => function($q) {
        //     $q->orderBy('sort_order', 'asc');
        // }])->where('id', $request->id)->first();

        $album = GalleryAlbum::with('gallery_images')->where('id', $request->id)->first();

        return response()->json($album, 200);
    }

    public function saveEditedGalleryAlbum(Request $request)
    {
        $album = GalleryAlbum::find($request->id);
        $rules = array();
        $this->validate($request, $rules);
        $album->name = $request->name;
        $album->description = $request->description;
        $album->album_image = $request->album_image;
        $album->save();
        return response()->json($album, 200);
    }

    public function deleteGalleryAlbum(Request $request)
    {
        $album_to_delete = GalleryAlbum::find($request->id);
        $album_to_delete->delete();
        return response()->json($album_to_delete, 200);
    }

    // Catalog
    public function getAlbums()
    {
        $albums = GalleryAlbum::orderBy('created_at', 'desc')->paginate(12);
        return response()->json($albums, 200);
    }

    public function getAlbum($id)
    {
        $album = GalleryAlbum::with(['gallery_images' => function($q) {
            $q->orderBy('sort_order', 'asc');
        }])->where('id', $id)->first();
        
        return response()->json($album, 200);
    }


}