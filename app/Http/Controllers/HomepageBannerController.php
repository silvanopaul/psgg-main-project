<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HomepageBanner;

class HomepageBannerController extends Controller
{
    public function showAllHomeBanners(){
        $banners = HomepageBanner::orderBy('id','desc')->get();        
        return $banners;
    }

    public function addHomeBanner(Request $request){
        $rules = [
            'title' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
            'description' => 'nullable|regex:/^[a-zA-Z0-9-.,+()\n ]+$/|max:1000',
            'theme' => 'required',
            'text_position' => 'required',
            'to_display' => 'boolean',
            'image_path' => 'required',
            'sort_order' => 'nullable|numeric',
        ];

        $this->validate($request, $rules);
        $new_banner = new HomepageBanner;
        $new_banner->title = $request->title;
        $new_banner->description = $request->description;
        $new_banner->theme = $request->theme;
        $new_banner->text_position = $request->text_position;
        $new_banner->to_display = $request->to_display;
        $new_banner->image_path = "/uploads/".$request->image_path;
        $new_banner->sort_order = $request->sort_order;
        $new_banner->save();

        return $new_banner;  
    }

    public function saveEditedHomeBanner(Request $request){
        $banner = HomepageBanner::find($request->id);
        $rules = [
            'title' => 'required|regex:/^[a-zA-Z0-9\s ]+$/',
            'description' => 'nullable|regex:/^[a-zA-Z0-9-.,+()\n ]+$/|max:1000',
            'theme' => 'required',
            'text_position' => 'required',
            'to_display' => 'boolean',
            'image_path' => 'required',
            'sort_order' => 'nullable|numeric',
        ];

        $this->validate($request, $rules);
        $banner->title = $request->title;
        $banner->description = $request->description;
        $banner->theme = $request->theme;
        $banner->text_position = $request->text_position;
        $banner->to_display = $request->to_display;
        $banner->image_path = $request->image_path;
        $banner->sort_order = $request->sort_order;
        $banner->save();
        return;
    }

    public function deleteHomeBanner(Request $request){
        $bannerToDelete = HomepageBanner::find($request->id);
        $bannerToDelete->delete();
        return $bannerToDelete;
    }

    public function getHomeBanners() {
        $banners = HomepageBanner::orderBy('sort_order','asc')->where('to_display', '1')->get();        
        return $banners;
    }
}
