<?php

namespace App\Http\Controllers;
use App\Models\SocialMedia;

use Illuminate\Http\Request;

class SocialMediaController extends Controller
{
    public function showAllSocials(){
        $socials = SocialMedia::orderBy('id','desc')->get();
        return $socials;     
    }

    public function addSocial(Request $request){
        $rules = [
            "name" => "required",
            "link" => "required",
            "image_path" => "required",
        ];

        $this->validate($request, $rules);
        $new_social = new SocialMedia;
        $new_social->name = $request->name;
        $new_social->link = $request->link;
        $new_social->image_path = "/uploads/".$request->image_path;
        $new_social->setting_id = $request->setting_id;
        $new_social->save();

        return $new_social; 
    }

    public function saveEditedSocial(Request $request){
        $social = SocialMedia::find($request->id);        
        $rules = [
            "name" => "required",
            "image_path" => "required",
            "link" => "required",
        ];
        $this->validate($request, $rules);
        $social->name = $request->name;
        $social->image_path = $request->image_path;
        $social->link = $request->link;
        $social->save();
        return;
    }

    public function deleteSocial(Request $request){
        $socialToDelete = SocialMedia::find($request->id);
        $socialToDelete->delete();
        return $socialToDelete;
    }
}
