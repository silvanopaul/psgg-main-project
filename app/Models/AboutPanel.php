<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutPanel extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'about_id'];

    public function about_panel_sections(){
        return $this->hasMany(AboutPanelSection::class);
    }

    public function about(){
        return $this->belongsTo(About::class);
    }
}
